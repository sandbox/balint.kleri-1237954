WHERE'S THE CODE?
-----------------

You've just checked out the code from Git's "master" branch. In Drupal contrib
module development, the code lives in branches that match a naming convention
which mirrors the released version numbers. In this project (since this is not
a contrib module) there are no released version numbers, instead, the available
branches are:

  gsoc
  dev

The "gsoc" branch contains my Google Summer of Code work, and the "dev" branch
holds the further development, based on my GSoC work. It's possible that the
repository contains addiotional branches for experimental features/solutions.

To see a list of all branches for this project using Git on the command line,
use the following command:

  git branch -a

To switch from "master" branch to the "dev" branch using Git on the command line,
use the following command:

  git checkout dev
